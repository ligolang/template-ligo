# A LIGO template on Gitpod

This is a [LIGO](https://ligolang.org/) template configured for ephemeral development environments on [Gitpod](https://www.gitpod.io/).

It provides the necessary tools for developing smart contracts in LIGO.

The `/contracts` directory contains an example contract (Counter), the contract is avaliable in all 4 flavour for LIGO (JsLIGO, PascaLIGO, CameLIGO, ReasonLIGO) 

To compile a contract to [Michelson](https://tezos.gitlab.io/active/michelson.html) use,
```sh
ligo compile contract contracts/counter.ligo
```

The `/tests` directory contains tests for the example contract written using the [LIGO testing framework](https://ligolang.org/docs/advanced/testing), to run the tests
```sh
ligo run test tests/counter.test.ligo
```

## Next Steps

Click the button below to start a new development environment:

[![Open in Gitpod](https://gitpod.io/button/open-in-gitpod.svg)](https://gitpod.io/#https://gitlab.com/ligolang/template-ligo)

## Get Started With Your Own Project

### A new project

Click the above "Open in Gitpod" button to start a new workspace. Once you're ready to push your first code changes, Gitpod will guide you to fork this project so you own it.

### An existing project

To get started with LIGO on Gitpod, copy the contents of this folder to your own project. To learn more, please see the [Getting Started](https://www.gitpod.io/docs/getting-started) documentation.