#!/usr/bin/env bash
cd "$(dirname "${BASH_SOURCE[0]}")"

VERSION_REGEX_PATTERN="[0-9]+.[0-9]+\.[0-9]+"
NEW_VERSION=$1

COMMAND=(sed -i)
if [ "$(uname)" == "Darwin" ]; then
    # Sed is different in macos. https://stackoverflow.com/questions/7573368/in-place-edits-with-sed-on-os-x 
    COMMAND=(sed -i '')
fi

"${COMMAND[@]}" -E "s|ligo:$VERSION_REGEX_PATTERN|ligo:$NEW_VERSION|g" ../../.gitpod/Dockerfile

