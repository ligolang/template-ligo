#import "../contracts/counter.mligo" "Counter"

(* Tests for main access point *)

let test_initial_storage =
 let initial_storage = 42 in
 let (taddr, _, _) = Test.originate Counter.main initial_storage 0tez in
 assert (Test.get_storage taddr = initial_storage)

let test_increment =
 let initial_storage = 42 in
 let (taddr, _, _) = Test.originate Counter.main initial_storage 0tez in
 let contr = Test.to_contract taddr in
 let _ = Test.transfer_to_contract_exn contr (Increment (1)) 1mutez in
 assert (Test.get_storage taddr = initial_storage + 1)
