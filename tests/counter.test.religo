#import "../contracts/counter.religo" "Counter"

/* Tests for main access point */

let test_initial_storage = {
  let initial_storage = 42;
  let (taddr, _, _) = Test.originate(Counter.main, initial_storage, 0tez);
  assert (Test.get_storage(taddr) == initial_storage)
};

let test_increment = {
  let initial_storage = 42;
  let (taddr, _, _) = Test.originate(Counter.main, initial_storage, 0tez);
  let contr = Test.to_contract(taddr);
  let _ = Test.transfer_to_contract_exn(contr, (Increment (1)), 1mutez);
  assert (Test.get_storage(taddr) == initial_storage + 1)
};