#import "../contracts/counter.ligo" "Counter"

(* Tests for main access point *)

const test_initial_storage =
  block {
    const initial_storage = 42;
    const (taddr, _, _) = Test.originate(Counter.main, initial_storage, 0tez);
    const storage = Test.get_storage(taddr);
  } with (storage = initial_storage);

const test_increment =
  block {
    const initial_storage = 42;
    const (taddr, _, _) = Test.originate(Counter.main, initial_storage, 0tez);
    const contr = Test.to_contract(taddr);
    const _ = Test.transfer_to_contract_exn(contr, Increment(1), 1mutez);
    const storage = Test.get_storage(taddr);
  } with (storage = initial_storage + 1);
